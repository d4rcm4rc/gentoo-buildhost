from collections import namedtuple
import functools
import re
import os

# TODO: replace hacks around namedtuple with proper classes

PackageVU = namedtuple('PackageVU', ('cat', 'p', 'pvr', 'use'))

@property
def _packageV(self):
    return PackageV(*(self[:3]))

@property
def _package(self):
    return Package(*(self[:2]))

def _withUse(self, use):
    return PackageVU(*self, use)

@property
def _emergeAtom(self):
    return f'={self.cat}/{self.p}-{self.pvr}'

@property
def _pf(self):
    return f'{self.cat}/{self.p}-{self.pvr}'

@property
def _activeUse(self):
    return tuple(u for u in self.use if not u.startswith('-'))

PackageVU.packageV = _packageV
PackageVU.package = _package
PackageVU.emergeAtom = _emergeAtom
PackageVU.pf = _pf
PackageVU.activeUse = _activeUse

#fix the use flags to a tuple
_pvunew_orig = PackageVU.__new__
@functools.wraps(_pvunew_orig)
def _pvunew(self, cat, p, pvr, use):
    return _pvunew_orig(self, cat, p, pvr, tuple(sorted(use)))
PackageVU.__new__ = _pvunew

PackageV = namedtuple('PackageV', ('cat', 'p', 'pvr'))

PackageV.package = _package
PackageV.withUse = _withUse
PackageV.emergeAtom = _emergeAtom
PackageV.pf = _pf

@property
def _filenameV(self):
    return f'{self.cat}{os.path.sep}{self.p}{os.path.sep}{self.p}-{self.pvr}'
PackageV.filename = _filenameV

Package = namedtuple('Package', ('cat', 'p'))

@property
def _filename(self):
    return f'{self.cat}{os.path.sep}{self.p}'
Package.filename = _filename

def parsePF(pf):
    m = re.match(r'^(.*)/(.*)-([0-9].*)$', pf)
    cat = m.group(1)
    p = m.group(2)
    v = m.group(3)
    return PackageV(cat, p, v)
