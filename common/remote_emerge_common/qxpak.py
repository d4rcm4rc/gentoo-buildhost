import os
import subprocess
import shlex

import remote_emerge_common.package as package

def get_useflags_direct(xpakPath):
    xpakPath = shlex.quote(xpakPath)
    p = subprocess.run('qtbz2 -sxO '+xpakPath+' | qxpak -xO - USE',
                    shell=True, capture_output=True, text=True)
    useflags = p.stdout.strip().split(' ')
    useflags.sort()
    return useflags

class Xpak:
    def __init__(self, xpakPath):
        self.xpakPath = xpakPath
        p = subprocess.run(['qtbz2', '-sxO', xpakPath], capture_output=True)
        p.check_returncode()
        if p.stderr:
            raise Exception(p.stderr.decode('utf-8'))
        self.xpakData = p.stdout
        self.dataCache = {}

    def getData(self, attr):
        try:
            return self.dataCache[attr]
        except KeyError:
            pass
        p = subprocess.run(['qxpak', '-xO', '-', attr],
                        input=self.xpakData, capture_output=True)
        p.check_returncode()
        data = p.stdout.decode('utf-8').strip()
        self.dataCache[attr] = data
        return data

    def _sortedUse(self):
        try:
            return self.dataCache[' _\nsortedUse']
        except KeyError:
            pass
        data = self.use.split(' ')
        data.sort()
        data = tuple(data)
        self.dataCache[' _\nsortedUse'] = data
        return data

    def filterRelevantUse(self, use):
        iuse = self.iuse
        #TODO: find a better approximation
        iuse = tuple(u for u in iuse if not u.startswith('kernel_'))
        return tuple(u for u in use if u in iuse)

    def __getattr__(self, attr):
        if attr == 'sortedUse':
            return self._sortedUse()
        return self.getData(attr.upper())

def find_package_for_useflags(path, pkg: package.PackageVU):
    pf = '-'.join([pkg.p, pkg.pvr])
    try:
        files = [f for f in os.listdir(os.path.join(path,pkg.package.filename))
                         if f.endswith('.xpak')]
    except FileNotFoundError:
        return None
    for f in files:
        xpak = Xpak(os.path.join(path, pkg.package.filename, f))
        if pf != xpak.pf:
            continue
        if xpak.filterRelevantUse(xpak.sortedUse) == xpak.filterRelevantUse(pkg.activeUse):
            return f
    return None
