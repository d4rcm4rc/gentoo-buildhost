import redis
import json

class RedisProvider:
    def __init__(self, conn: redis.StrictRedis):
        self._conn = conn

    def store_job(self, target: str, jobid: int, job: dict):
        encoded_job = json.dumps(job)
        self._conn.set(f'jobs.{target}.{jobid}.jobdata', encoded_job)

    def set_key(self, target: str, jobid: int, key: str):
        self._conn.set(f'jobs.{target}.{jobid}.key', key)

    def get_next_jobid(self, target: str) -> int:
        return self._conn.incr(f'jobs.{target}.jobid')

    def get_log_lines(self, target: str, jobid: int,
                      start: int, limit: int) -> list:
        return self._conn.lrange(
            f'jobs.{target}.{jobid}.output',
            start,
            start+limit-2)

    def count_log_lines(self, target: str, jobid: int) -> int:
        return self._conn.llen(f'jobs.{target}.{jobid}.output')
