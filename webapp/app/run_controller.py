from flask import Blueprint, jsonify, abort, request

import config
from job import Job
from remote_emerge_common.qxpak import find_package_for_useflags
import remote_emerge_common.package as package
import validate
import docker

def run_controller(docker: docker.Docker):
    blueprint = Blueprint('run_controller', __name__)

    @blueprint.route('/<target>/run', methods=['POST'])
    def run(target):
        cfg = config.get_config()
        gc = cfg.get('general', {})
        base = gc.get('internal-base-url', '')
        packagePath = config.target_path(target)

        try:
            tgconfig = cfg['targets'][target]
        except KeyError:
            abort(404)

        if docker.isNotRunning(target).returncode:
            return jsonify({
                'status': 'busy',
                'output': ''}), 500

        return extract_job(packagePath, request)

    return blueprint

def extract_job(packagePath: str, request):
    job = Job()

    try:
        data = request.json
        for d in data:
            cat = d['cat']
            p   = d['p']
            pvr = d['pvr']
            use = d['use']
            validate.category(cat)
            validate.package(p)
            validate.pvr(pvr)
            validate.use(use)
            pkg = package.PackageVU(cat, p, pvr, use)
            if find_package_for_useflags(packagePath, pkg):
                continue
            job.add_packageVU(pkg)
    except validate.ValidationException as e:
        abort(400, str(e))
    except Exception as e:
        raise e
        abort(400)
    return jsonify(job.__dict__)
