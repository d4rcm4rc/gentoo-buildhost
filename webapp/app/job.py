import copy

from remote_emerge_common.package import PackageVU

class Job:
    CONFIG_KEYS = {
        'make.conf': 'make_conf',
        'target-make.conf': 'target_make_conf',
    }

    def __init__(self):
        self.packages = []
        self.package_use = ''
        self.make_conf = ''
        self.target_make_conf = ''

    @property
    def __dict__(self) -> dict:
        return {
            'packages': self.packages,
            'package.use': self.package_use,
            'make.conf': self.make_conf,
            'target-make.conf': self.target_make_conf,
        }

    def add_package(self, package: str):
        self.packages.append(package)

    def add_package_use(self, pf: str, uses: str):
        self.package_use = "\n".join(filter(None, (
            self.package_use,
            f'{pf} {uses}')))

    def add_packageVU(self, package: PackageVU):
        self.add_package(package.emergeAtom)
        self.add_package_use(package.emergeAtom, ' '.join(package.use))

    def build_full_job(self, cfg: dict, target: str):
        tc = cfg['targets'][target]

        full_job = copy.copy(self)
        full_job.package_use = "\n".join(filter(None, (
            tc.get('package.use', None),
            self.package_use)))

        for key, member in self.__class__.CONFIG_KEYS.items():
            setattr(full_job, member, tc.get(key, ''))

        return full_job
