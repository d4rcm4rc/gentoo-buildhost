from flask import Flask, jsonify, Response, abort, request, send_file
import os
from config import get_config, target_path
import subprocess
import json
from urllib.parse import unquote
from remote_emerge_common.qxpak import find_package_for_useflags
import remote_emerge_common.package as package
import validate
import docker

from run_controller import run_controller

import yaml

app = Flask(__name__)

"""
@app.route('/')
def hello_world():
    import subprocess
    p = subprocess.run(['docker', 'images'], capture_output=True, text=True)
    return Response(p.stdout, mimetype='text/plain')
"""

@app.route('/ct')
def config_test():
    return jsonify(get_config())

@app.route('/dperms')
def ep():
    p = subprocess.run(['docker', 'images'], capture_output=True, text=True)
    return Response(p.stdout + p.stderr, mimetype='text/plain')

@app.route('/version')
def version():
    return '0.1'

@app.route('/<target>/pkgs', methods=['POST'])
def getPackageAvailabilities(target):
    path = target_path(target)
    try:
        data = request.json
        res = {'available': [],
            'unavailable': []}
        for d in data:
            cat = d['cat']
            p   = d['p']
            pvr = d['pvr']
            use = d['use']
            pkg = package.PackageVU(cat, p, pvr, use)
            pkgFile = find_package_for_useflags(path, pkg)
            res['available' if pkgFile else 'unavailable'].append(d)
        return jsonify(res)
    except Exception as e:
        print(e)
        abort(400)

@app.route('/<target>/pkg/<cat>/<p>/<pvr>')
def downloadPkg(target, cat, p, pvr):
    cfg = get_config()
    try:
        tgconfig = cfg['targets'][target]
    except KeyError:
        abort(404)
    use = unquote(request.query_string.decode('utf-8')).split(' ')
    path = target_path(target)
    pkg = package.PackageVU(cat, p, pvr, use)
    print(path, pkg)
    pkgFile = find_package_for_useflags(path, pkg)
    if pkgFile:
        attachment_filename = f'{p}-{pvr}.xpak'
        return send_file(os.path.join(path,cat,p,pkgFile),
                        as_attachment=True,
                        attachment_filename=attachment_filename)
    else:
        abort(404)

@app.route('/<target>/status')
def status(target):
    cfg = get_config()
    try:
        tgconfig = cfg['targets'][target]
    except KeyError:
        abort(404)
    return str(docker('isNotRunning', target).returncode)
    #return jsonify(tgconfig)

def buildJob(cfg, target, job):
    tc = cfg['targets'][target]
    fullJob = {}
    fullJob['packages'] = job['packages']
    fullJob['package.use'] = "\n".join(filter(None, (
        job.get('package.use', None),
        tc.get('package.use', None))))
    for key in ('make.conf', 'target-make.conf'):
        fullJob[key] = tc.get(key,'')
    return fullJob

def storeJob(target, job):
    import redis
    import string
    import json
    import random
    r = redis.StrictRedis(host='redis', port=6379, db=0)
    key = ''.join(random.SystemRandom() \
                        .choices(string.ascii_lowercase + string.digits, k=32))
    r.set(f'jobs.{target}.{key}', json.dumps(job))
    return (target, key)

def retrieveJob(target, key):
    import redis
    import json
    r = redis.StrictRedis(host='redis', port=6379, db=0)
    p = r.pipeline()
    data = p.get(f'jobs.{target}.{key}')
    p.delete(f'jobs.{target}.{key}')
    return json.loads(p.execute()[0])

"""
@app.route('/<target>/run', methods=['POST'])
def run(target):
    cfg = get_config()
    gc = cfg.get('general',{})
    base = gc.get('internal-base-url', '')
    path = target_path(target)
    try:
        tgconfig = cfg['targets'][target]
    except KeyError:
        abort(404)
    if docker('isNotRunning', target).returncode:
        return jsonify({
            'status': 'busy',
            'output': ''}), 500
    job = {'packages':[]}
    try:
        data = request.json
        for d in data:
            cat = d['cat']
            p   = d['p']
            pvr = d['pvr']
            use = d['use']
            validate.category(cat)
            validate.package(p)
            validate.pvr(pvr)
            validate.use(use)
            pkg = package.PackageVU(cat, p, pvr, use)
            if find_package_for_useflags(path, pkg):
                continue
            uses = ' '.join(use)
            job['packages'].append(pkg.pf)
            job.setdefault('package.use','')
            job['package.use'] += f'{pkg.pf} {uses}\n'
    except validate.ValidationException as e:
        abort(400, str(e))
    except Exception as e:
        raise e
        abort(400)
    if not job['packages']:
        return jsonify({'status': 'no_work', 'output': ''})
    job = buildJob(cfg, target, job)
    job = {'job': job}
    jobgetter = storeJob(target, job)
    jobkey = '.'.join(jobgetter)
    joburl = f'http://{base}/job/{jobkey}'
    return jsonify(joburl)
    p = docker('startWorker', target, joburl)
    if p.returncode == 0:
        return jsonify({
            'status': 'started',
            'output': p.stdout})
    else:
        return jsonify({
            'status': 'error',
            'output': p.stderr}), 500
        #return Response(str(p.stderr), status=500, mimetype='text/plain')
"""

@app.route('/job/<key>')
def getJob(key):
    validate.jobkey(key)
    job = retrieveJob(*key.split('.'))
    return jsonify(job)

docker = docker.Docker(app.root_path)
app.register_blueprint(run_controller(docker))
