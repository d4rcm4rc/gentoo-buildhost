import os
import yaml

_config_cache = None

def _get_config():
    cfgpath = os.getenv('CONFIG_YML_PATH', '/config.yml')
    with open(cfgpath, 'r') as stream:
        return yaml.safe_load(stream)

def get_config():
    global _config_cache
    if _config_cache is None:
        _config_cache = _get_config()
    return _config_cache

def target_path(target):
    return f'/packages/{target}/target'
