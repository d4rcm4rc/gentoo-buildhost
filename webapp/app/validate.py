from typing import Iterable
import functools
import re

class ValidationException(Exception):
    pass

def _want_string(f):
    @functools.wraps(f)
    def w(data):
        if not isinstance(data, str):
            raise ValidationException('Must be a string')
        return f(data)
    return w

@_want_string
def category(c: str):
    if not re.match(r'[a-z0-9]{2,}-[a-z0-9]{2,}', c):
        raise ValidationException('Invalid category')

@_want_string
def package(p: str):
    if not re.match(r'[A-Za-z][A-Za-z0-9-_]*', p):
        raise ValidationException('Invalid package')

@_want_string
def pvr(version: str):
    if not re.match(r'[0-9][0-9a-zA-Z.-]*', version):
        raise ValidationException('Invalid version')

def use(use: Iterable[str]):
    @_want_string
    def single_use(u: str):
        if not re.match(r'-?[a-zA-Z][a-zA-Z0-9-_]*', u):
            raise ValidationException('Invalid use flag')
    for u in use:
        single_use(u)

@_want_string
def jobkey(key: str):
    if not re.match(r'[a-z]+\.[a-z0-9]{32}', key):
        raise ValidationException('invalid job key')
