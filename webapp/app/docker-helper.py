import sys
import os

from config import get_config
import subprocess
import json

gadgets = {}

def gadget(f):
    gadgets[f.__name__.lower()] = f

def docker(*args, input=None):
    p = subprocess.run(['docker'] + list(args),
                       capture_output=True,
                       text=True, input=input)
    if p.stderr:
        raise Exception(p.stderr)
    return p.stdout.strip()

@gadget
def testJob(target, job):
    cfg = get_config()
    print(json.dumps(buildJob(cfg, target, job)))

@gadget
def isNotRunning(target):
    cfg = get_config()
    containerName = cfg['targets'][target]['container-name']
    res = docker('ps', '--format', '{{.ID}}', '--no-trunc',
                 '--filter', 'name=^/'+containerName+'$')
    return bool(res)

@gadget
def startWorker(target, jobData):
    cfg = get_config()
    gc = cfg.get('general',{})
    tc = cfg['targets'][target]
    containerName = tc['container-name']
    imageName = tc['image-name']
    hostPackagePath = tc['host-pkg-path']
    targetPackagePath = tc['target-pkg-path']
    portageTree = cfg['general']['portage-tree']
    args = []

    #make it findable
    args.extend(['--name', containerName])
    #the host's portage tree
    args.extend(['--mount',
                 'type=bind,source=' + portageTree +
                 ',target=/usr/portage,readonly'])
    #we want to build in ram
    args.extend(['--tmpfs', '/var/tmp/portage:exec'])
    #link the binary paths; for buildhost-packages and target-packages
    args.extend(['--mount',
                 'type=bind,source=' + hostPackagePath +
                 ',target=/usr/portage_packages_host'])
    args.extend(['--mount',
                 'type=bind,source=' + targetPackagePath +
                 ',target=/usr/portage_packages_target'])
    args.extend(['--network',
                 gc.get('builder-network', 'bridge')])
    args.extend(tc.get('builder-docker-args', []))

    docker('run', '-d', '--rm', *args, imageName, jobData)

def main():
    if len(sys.argv) < 2:
        print('Need gadget', file=sys.stderr)
        exit(-2)
    cmd = sys.argv[1]
    try:
        g=gadgets[cmd.lower()]
    except:
        print('Unknown gadget', file=sys.stderr)
        exit(-2)
    try:
        return g(*sys.argv[2:])
    except Exception as e:
        import traceback
        traceback.print_exc()
        exit(-1)

if __name__ == '__main__':
    exit(main())
