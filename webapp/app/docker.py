import os
import subprocess
import functools

class Docker:
    def __init__(self, root_path: str):
        self.root_path = root_path

    def _run_docker(self, cmd, *args):
        return subprocess.run([
            'sudo', 'python3',
            os.path.join(self.root_path, 'docker-helper.py'),
            cmd, *args
        ], capture_output=True, text=True)

    def __getattr__(self, name):
        return functools.partial(self._run_docker, name)
