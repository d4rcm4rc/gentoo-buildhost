import collections
import random
import string

StoreJobResult = collections.namedtuple('StoreJobResult', ['target', 'jobid', 'key'])

class WorkerCommunicatorService:
    def __init__(self, provider):
        self._provider = provider
        self._random = random.SystemRandom()

    def add_job(self, target: str, job: dict) -> StoreJobResult:
        jobid = self._provider.get_next_jobid(target)
        key = self._generate_key()
        self._provider.store_job(target, jobid, job)
        self._provider.set_key(target, jobid, key)

        return StoreJobResult(target, jobid, key)

    def get_log_lines(self, target: str, jobid: int,
                      start: int, limit: int) -> list:
        return self._provider.get_log_lines(target, jobid, start, limit)

    def count_log_lines(self, target: str, jobid: int) -> int:
        return self._provider.count_log_lines(target, jobid)

    def _generate_key(self):
        return ''.join(self._random \
                       .choices(string.ascii_lowercase + string.digits, k=32))
