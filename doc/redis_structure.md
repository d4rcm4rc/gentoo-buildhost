Redis Key Structure
===================

Redis currently stores only information related to single jobs.
A job can be identified by its target and a target-unique job id.
A job shall only be accessible if a key is known for this job.

### jobs.{target}.jobid

The next jobid to be used.
Call `INCR` on this value to get a new jobid. Redis will handle the default value and atomic increment.

### jobs.{target}.{jobid}.key

The key for authentication is stored here.

### jobs.{target}.{jobid}.jobdata

A json string representing a build job is stored here.

### jobs.{target}.{jobid}.status

A string representing the job's status is stored here (not yet used).

### jobs.{target}.{jobid}.output

A list of tuples `(stream, line)`. Stores the job's output. `stream` is an integer representing the file descriptor (1 - stdout, 2 - stderr), `line` is the line (or less than a line if the line is too long).
