#!/bin/env python3

import json

from config import Config
from remote_emerge_common.package import PackageVU
import error
import parse_args
import actions

cfg = Config()
cfg.fixConfigroot()

parser = parse_args.parser
args = parser.parse_args()
packages = args.packages

actioner = actions.RemoteEmerge(cfg)

# collect information on packages, build them remotely if necessary, install
# dependencies locally, fetch remote packages, install remote packages,
# delete downloaded remote packages
def defaultTask():
    actioner.check_root()
    print('Checking correct server version...')
    actioner.check_version()
    print('Getting complete package specifications...')
    pkgs = actioner.pretend_emerge(packages)
    if not pkgs:
        raise error.NothingToDo()
    print(pkgs)
    print('Checking remote availabilities...')
    avail, needbuild = actioner.check_availabilities(pkgs)
    print(f'Building packages remotely... {needbuild}')
    actioner.remote_build(needbuild)
    print('Installing dependencies')
    actioner.emerge_deps(pkgs).check_returncode()
    print('Waiting for remote emerge to finish')
    actioner.wait_run_complete()
    try:
        print('Downloading binary packages from remote...')
        files = actioner.download_store_packages(pkgs)
    except error.PackageDownloadError as e:
        actioner.delete_packages(e.files)
        raise
    try:
        print('Installing binary packages...')
        actioner.emerge_binpkgs(pkgs)
    finally:
        print('Deleting packages')
        actioner.delete_packages(files)

def printPkgTask():
    print('Checking correct server version...')
    actioner.check_version()
    print('Getting complete package specifications...')
    pkgs = actioner.pretend_emerge(packages)
    if not pkgs:
        raise error.NothingToDo()
    print('Checking remote availabilities...')
    avail, needbuild = actioner.check_availabilities(pkgs)
    print(json.dumps(needbuild))

defaultTask()
#printPkgTask()
