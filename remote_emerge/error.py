class PackageVUParseException(Exception):
    pass

class InvalidRemotePackage(Exception):
    def __init__(self, message, package):
        super().__init__(message)
        self.package = package

class NoXpakFilename(Exception):
    pass

class PackageDownloadError(Exception):
    def __init__ (self, message, files):
        super().__init__(message)
        self.files = files

class StartJobException(Exception):
    pass

class MustBeRoot(Exception):
    pass

class NothingToDo(Exception):
    pass
