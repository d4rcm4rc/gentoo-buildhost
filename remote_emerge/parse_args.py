import argparse
import sys

parser = argparse.ArgumentParser(description=
    'Run emerges remotely and coordinate the ' \
    'installation of remotely built packages.')

parser.add_argument('packages', metavar='pkg', type=str, nargs='+',
                    help='The packages')
