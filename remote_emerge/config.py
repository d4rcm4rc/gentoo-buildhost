import os
from urllib.parse import urljoin
import requests

from portage.util import getconfig as portage_getconfig

class Config:
    def __init__(self):
        cfgroot = os.environ.get('PORTAGE_CONFIGROOT', '/')
        make_conf_paths = [
            os.path.join(cfgroot, 'etc', 'make.conf'),
            os.path.join(cfgroot, 'etc', 'portage', 'make.conf')
        ]
        try:
            if os.path.samefile(*make_conf_paths):
                make_conf_paths.pop()
        except OSError:
            pass
        make_conf_paths.append(os.path.join(
            cfgroot,
            'etc',
            'portage',
            'remote_emerge.conf'))
        make_conf = {}
        for x in make_conf_paths:
            mygcfg = portage_getconfig(x,
                allow_sourcing=True,
                expand=make_conf,
                recursive=True)
            if mygcfg is not None:
                make_conf.update(mygcfg)
        self.cfg = make_conf
        self.requester = Requester(make_conf)

    def fixConfigroot(self):
        orig = os.environ.get('ORIG_PORTAGE_CONFIGROOT', self.cfg.get(
            'ORIG_PORTAGE_CONFIGROOT'))
        if orig:
            os.environ['PORTAGE_CONFIGROOT'] = orig

    def __getitem__(self, key):
        return self.cfg[key]

class Requester:
    __slots__ = ('cfg')

    def __init__(self, cfg: dict):
        self.cfg = cfg

    def __getattr__(self, name: str):
        prepend=()
        if name.startswith('target'):
            name = name[6].lower() + name[7:]
            prepend=(self.cfg['REMOTE_TARGET'],)
        def caller(url, *args, **kwargs):
            url = '/'.join((
                self.cfg['REMOTE_SERVER'].rstrip('/'),
                *prepend,
                url))
            kwargs.setdefault('auth', (self.cfg['REMOTE_USERNAME'],
                self.cfg['REMOTE_PASSWORD']))
            kwargs.setdefault('headers', {}) \
                  .setdefault('Accept', 'application/json')
            m = getattr(requests, name)
            return m(url, *args, **kwargs)
        return caller
