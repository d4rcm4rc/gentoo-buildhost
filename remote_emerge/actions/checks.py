import os

import error
from config import Config

OKAY_VERSION = (0,1)

def compare_version(ver):
    try:
        ver = tuple(map(int, (ver.split("."))))
        if ver[0] != OKAY_VERSION[0]:
            return False
        return ver >= OKAY_VERSION
    except:
        return False

def check_version(cfg: Config):
    r = cfg.requester.get('version', auth=None)
    r.raise_for_status()
    ver = r.text

    return compare_version(ver)

def check_root(cfg: Config):
    if os.geteuid() != 0:
        raise error.MustBeRoot()
