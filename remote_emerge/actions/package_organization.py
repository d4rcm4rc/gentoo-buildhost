from typing import Iterable
import os

from config import Config
from remote_emerge_common.package import PackageVU, Package
import error

def write_package(cfg: Config, pkg: PackageVU, data):
    filenameRaw = os.path.join(cfg['PKGDIR'], pkg.packageV.filename)
    make_pkgdir(cfg, pkg.package)

    filename = find_xpak_filename(filenameRaw)

    f = open(filename, 'wb')
    try:
        for chunk in data:
            f.write(chunk)
    except Exception as e:
        f.close()
        os.remove(filename)
        raise e
    f.close()
    return filename

def make_pkgdir(cfg: Config, pkg: Package = None):
    if pkg is None:
        os.makedirs(cfg['PKGDIR'], exist_ok=True)
    else:
        os.makedirs(os.path.join(cfg['PKGDIR'], pkg.filename), exist_ok=True)

def find_xpak_filename(filenameRaw):
    counter = 1
    while counter < 10000:
        filename = f'{filenameRaw}-{counter}.xpak'
        if not os.path.lexists(filename):
            return filename
        counter += 1
    raise error.NoXpakFilename(filenameRaw)

def delete_packages(cfg: Config, files: Iterable[str]):
    for f in files:
        os.remove(f)
