import shlex
import re
import os

from typing import Iterable, Union
import subprocess

from config import Config
import remote_emerge_common.package as package
import error

def parse_output(line: str):
    line = line.strip()
    line = re.sub(r'^\[ebuild .{7}\] (.*) +[0-9.]+ [KMG]iB$', r'\1', line)

    tkns = shlex.split(line)

    pf = re.sub(r'::.*',r'',tkns[0])
    pkg = package.parsePF(pf)

    all_flags = []

    for t in tkns[1:]:
        if re.match(r'\[.+::.+\]', t):
            continue
        m = re.match(r'([A-Z0-9_]+)=(.*)', t)
        if not m:
            print(t)
            raise error.PackageVUParseException(
                'Couldn\'t parse use flags',
                line)
        type = m.group(1)
        flags = m.group(2)

        sflags = shlex.split(flags)
        if type == 'USE':
            prefix = ''
        else:
            prefix = type.lower() + '_'

        for f in sflags:
            f = re.sub(r'[\[\(\{\]\)\}]',r'', f)
            f = re.sub(r'^(-?)',r'\g<1>'+prefix, f)
            f = re.sub(r'^(.*)%$',r'\g<1>', f)
            all_flags.append(f)
    return pkg.withUse(all_flags)

def pretend_emerge(cfg: Config, atoms: Iterable[str]):
    p = subprocess.run(
        ('emerge', '-pv', '--nodeps', *atoms),
        stdout=subprocess.PIPE)

    out = p.stdout.decode('utf-8')
    lines = out.split('\n')
    lines = [l for l in lines if re.match(
        r'^\[ebuild .{7}\] .* USE=.* [0-9\.]+ [KMG]iB$',l)]

    return [parse_output(l) for l in lines]

def emerge_deps(cfg: Config,
                atoms: Iterable[Union[package.PackageV,
                                      package.PackageVU]]):
    return subprocess.run(
        ('emerge', '--onlydeps', *(a.emergeAtom for a in atoms)))

def emerge_binpkgs(cfg: Config,
                   atoms: Iterable[Union[package.PackageV,
                                         package.PackageVU]]):
    oldpkgdir = os.environ.get('PKGDIR', None)
    os.environ['PKGDIR'] = cfg['PKGDIR']
    process = subprocess.run(
        ('emerge', '--usepkgonly', *(a.emergeAtom for a in atoms)))
    os.environ['PKGDIR'] = oldpkgdir
    return process
