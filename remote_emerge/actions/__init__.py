import importlib
import functools
import error

class RemoteEmerge:
    _actions = {
        'emerge_control': [
            'pretend_emerge',
            'emerge_binpkgs',
            'emerge_deps',
        ],
        'checks': [
            'check_version',
            'check_root',
        ],
        'package_download': [
            'download_package',
            'head_package',
            'check_availabilities',
            'remote_build',
            'wait_run_complete',
        ],
        'package_organization': [
            'write_package',
            'make_pkgdir',
            'delete_packages',
        ]
    }

    def __init__(self, cfg):
        self.cfg = cfg

    def download_store_package(self, pkg):
        p = self.download_package(pkg)
        return self.write_package(pkg, p)

    def download_store_packages(self, pkgs):
        pkgfiles = []
        for pkg in pkgs:
            try:
                f = self.download_store_package(pkg)
            except Exception as e:
                raise error.PackageDownloadError('Error downloading packages',
                                                 pkgfiles) from e
            pkgfiles.append(f)
        return pkgfiles

for mn,fs in RemoteEmerge._actions.items():
    for fn in fs:
        def make_wrapper():
            moduleName = mn
            functionName = fn
            def wf(self, *args, **kwargs):
                module = importlib.import_module('actions.'+moduleName)
                f = getattr(module, functionName)
                return f(self.cfg, *args, **kwargs)
            return wf
        setattr(RemoteEmerge, fn, make_wrapper())
