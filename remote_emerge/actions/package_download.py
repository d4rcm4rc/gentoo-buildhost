from typing import Iterable
import time
import json
import requests

from config import Config
from remote_emerge_common.package import PackageVU
import error


def head_package(cfg: Config, pkg: PackageVU):
    path = '/'.join(('pkg', *pkg.packageV))
    url = path + '?' + ' '.join(pkg.use)

    r = cfg.requester.targetHead(url)
    return r

def download_package(cfg: Config,
                     pkg: PackageVU,
                     *,
                     chunk_size : int = 1024*1024):
    path = '/'.join(('pkg', *pkg.packageV))
    url = path + '?' + ' '.join(pkg.use)

    r = cfg.requester.targetGet(url, stream=True)
    r.raise_for_status()
    return r.iter_content(chunk_size)

def check_availabilities(cfg: Config, pkgs: Iterable[PackageVU]):
    data = json.dumps([p._asdict() for p in pkgs])
    r = cfg.requester.targetPost('pkgs',
        headers={'Content-Type': 'application/json'},
        data = data)
    r.raise_for_status()
    jres = r.json()
    f = lambda d: PackageVU(**d)
    avail = list(map(f, jres['available']))
    unavail = list(map(f, jres['unavailable']))
    for d in (*avail, *unavail):
        if d not in pkgs:
            raise error.InvalidRemotePackage('Invalid package returned', d)
    return (avail, unavail)

def remote_build(cfg: Config, pkgs: Iterable[PackageVU]):
    data = json.dumps([p._asdict() for p in pkgs])
    r = cfg.requester.targetPost('run',
        headers={'Content-Type': 'application/json'},
        data = data)
    if r.status_code != requests.codes.ok:
        raise error.StartJobException(r.text)
    return json.loads(r.text)

def wait_run_complete(cfg: Config):
    while True:
        r = cfg.requester.targetGet('status')
        r.raise_for_status()
        if r.text == u'0':
            break
        time.sleep(1)
