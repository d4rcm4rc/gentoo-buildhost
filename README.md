Gentoo Buildhost
================

This project shall allow remote compilation of Gentoo's source-based ebuilds into compiled and target-optimized binary packages.

Motivation for this project is the usage of Gentoo on notebooks. They often have slower CPUs than a desktop computer, so the compilation takes more time. Also they may have less RAM, so compiling in a ramdisk may not be feasible for larger packages (think chromium, firefox or kicad).
So besides the wear on the (nowadays often non-ejectable) battery due to prolonged use and exposure to heat, compiling large packages also causes unnecessary writes to the notebook's SSD.
By simply sending a job like 'compile firefox and chromium' to a desktop, workstation or even a server, this can be avoided, as these big machines are faster, do not have a battery and have enough RAM to be able to avoid having to build on a hard disk or SSD.

This way, both the customizability offered by Gentoo, and fast and cool software installations just provided from binary distributions, can be achieved.

## Architecture

This system heavily uses Docker.
Not only are Docker and docker-compose used to deploy the controlling application, but also the worker that will build the actual packages is automatically spawned as a docker container.

Currently, two containers are constantly running:
- An nginx instance provides HTTP access to the web application. It shall terminate SSL connections in a production deployment
- A Python webapp using Flask (with the flask-debugserver when developing and uwsgi in production) handles a target's requested packages and compile jobs and will start a worker to do the compilation, if necessary.

The webapp contains the host's Docker socket as bind mount, so interaction with Docker from this container is painless.
To avoid having to run the webapp as root, it does all communication with the docker daemon through a small script, which can be run with elevated privileges through sudo. This hopefully reduces the attack surface to a minimum.

It interacts with with binary packages through [Gentoo's q applets](https://wiki.gentoo.org/wiki/Q_applets).

## Current status

This is a work in progress and not yet usable.

- Building packages works and built packages seem to work on the target machine
- The webapp for managing jobs still needs to be able to receive a compile job from the target machine
- A mechanism for copying the binary packages to the target machine is still missing
- Old builds are not yet cleaned up
- Packages are compiled again, even if this was not necessary. A mechanism to detect an existing binary package should not be difficult.
- The host must run Gentoo (or must at least provide a Portage tree and keep it updated), support for providing an own Portage tree is planned
- Currently, no packages missing from the host can be built, as the builder can not store any downloaded source packages
- To build the worker, a stage3 tarball needs to be placed into the `builder` directory. Eventually, downloading this tarball shall be integrated into the dockerfile, though
