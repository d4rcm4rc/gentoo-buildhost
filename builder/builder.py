#!/bin/env python3

import sys
import json
import subprocess
import time

jobdata = json.load(sys.stdin)

if 'packages' not in jobdata or not isinstance(jobdata['packages'], list):
    print('Please enter some packages to be emerged', file=sys.stderr)
    exit(-1)

# add data to make.conf and use flags
if 'make.conf' in jobdata:
    with open('/etc/portage/make.conf', 'a') as f:
        f.write('\n')
        f.write(jobdata['make.conf'])
        f.write('\n')
if 'package.use' in jobdata:
    with open('/etc/portage/package.use/target', 'w') as f:
        f.write(jobdata['package.use'])
if 'package.env' in jobdata:
    with open('/etc/portage/package.env/package.env', 'w') as f:
        f.write(jobdata['package.env'])

#make sure every dependency has its use flags and keywords set
subprocess.run(['emerge', '--autounmask-write', '--autounmask-only'] + jobdata['packages']).check_returncode()
subprocess.run(['etc-update', '--automode', '-5']).check_returncode()

time1 = time.time()

print(time.strftime('%a, %d %b %Y %H:%M:%S +0000',time.localtime(time1)))

#install all dependencies
subprocess.run(['emerge', '--onlydeps'] + jobdata['packages']).check_returncode()

#apply target settings
with open('/etc/portage/make.conf', 'a') as f:
    f.write('\n')
    if 'target-make.conf' in jobdata:
        f.write(jobdata['target-make.conf'])
        f.write('\n')
    f.write('PKGDIR=/usr/portage_packages_target')
    f.write('\n')

time2 = time.time()
print(time.strftime('%a, %d %b %Y %H:%M:%S +0000',time.localtime(time2)))

subprocess.run(['emerge', '--buildpkgonly'] + jobdata['packages'])

time3 = time.time()
print(time.strftime('%a, %d %b %Y %H:%M:%S +0000',time.localtime(time3)))

print('=============')
print('before emerging dependencies:', time.strftime('%a, %d %b %Y %H:%M:%S +0000',time.localtime(time1)))
print('before emerging packages:    ', time.strftime('%a, %d %b %Y %H:%M:%S +0000',time.localtime(time2)))
print('after  emerging packages:    ', time.strftime('%a, %d %b %Y %H:%M:%S +0000',time.localtime(time3)))
