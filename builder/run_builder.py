#!/bin/env python3

import sys
sys.path.append('/pypkg')

import asyncio
import requests
import json

joburl= sys.argv[1]

jobr = requests.get(joburl)
jobr.raise_for_status()

jobpkg = jobr.json()

job = jobpkg['job']
jobdata = json.dumps(job)

#todo: stdout and stdin stuff

async def run_builder(jobdata):
    p = await asyncio.create_subprocess_exec('/usr/sbin/builder',
                                             stdin=asyncio.subprocess.PIPE)
    p.stdin.write(jobdata.encode('utf-8'))
    p.stdin.close()
    await p.wait()

loop = asyncio.get_event_loop()
loop.run_until_complete(run_builder(jobdata))
