import functools
import json

from connection import Connection
from meta_handler import MetaHandler, messageAction, HandlerException

MSG_AUTH = 0
MSG_GETJOB = 1
MSG_LOG_LINE = 2

class InvalidKeyException(HandlerException):
    pass

def needAuth(f):
    @functools.wraps(f)
    async def wrapped(self, conn: Connection):
        if not self.isAuthenticated:
            return False
        return await f(self, conn)
    return wrapped

class Handler(metaclass=MetaHandler):
    def __init__(self, persistence):
        self.persistence = persistence
        self.isAuthenticated = False
        self.target = None
        self.jobid = 0

    @messageAction(MSG_AUTH)
    async def authenticate(self, conn: Connection):
        target = conn.readPrefixedStr(1)
        supplied_key = conn.readPrefixedStr(1)
        correct_key = self.persistence.get_key(target, self.jobid)
        if supplied_key != correct_key:
            raise InvalidKeyException
        self.isAuthenticated = True
        self.target = target
        return True

    @messageAction(MSG_GETJOB)
    @needAuth
    async def get_job(self, conn: Connection):
        job = self.persistence.get_job(self.target, self.jobid)
        data = json.dumps(job)
        await conn.writePrefixedStr(data)
        return True

    @messageAction(MSG_LOG_LINE)
    @needAuth
    async def log_line(self, conn: Connection):
        fd = await conn.readByte()
        line = await conn.readPrefixedStr(2)
        self.persistence.append_output(self.target, self.jobid, fd, line)
        return True
