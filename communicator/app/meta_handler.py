import functools

class MetaHandler(type):
    def __init__(cls, name, bases, attrs):
        handlers = {}
        for key, val in attrs.items():
            msgtype = getattr(val, 'msgtype', None)
            if msgtype is not None:
                handlers[msgtype] = val
        def get_action(self, msgtype):
            action = handlers.get(msgtype)
            if not action:
                return None # TODO: raise Exception
            return functools.partial(action, self)
        cls.get_action = get_action

def messageAction(msgtype):
    def decorator(f):
        f.msgtype = msgtype
        return f
    return decorator

class HandlerException(Exception):
    pass
