import connection
from meta_handler import HandlerException

#TODO: better error handling, logging
class Dispatcher:
    def __init__(self, handler):
        self.handler = handler

    async def run(self, conn: connection.Connection):
        while True:
            try:
                data = await conn.read(1)
            except connection.ConnectionClosedException:
                return
            try:
                res = await self.dispatch(data[0], conn)
            except connection.ConnectionException:
                res = False
            except HandlerException:
                res = False
            if not res:
                conn.close()
                return

    async def dispatch(self, msgtype: int, conn: connection.Connection):
        action = self.handler.get_action(msgtype)
        if not action:
            return False
        return await action(conn)
