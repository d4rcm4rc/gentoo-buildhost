import aioredis
import json

from provider import NotFoundException

class RedisProvider:
    def __init__(self, conn: aioredis.Redis):
        self._conn = conn

    async def get_key(self, target: str, jobid: int) -> str:
        return await self._conn.get(f'jobs.{target}.{jobid}.key')

    async def get_job(self, target: str, jobid: int) -> str:
        data = await self._conn.get(f'jobs.{target}.{jobid}.jobdata')
        if data is None:
            raise NotFoundException
        return json.loads(data.decode('UTF-8'))

    async def append_output(self, target: str, jobid: int, fd: int, line: str):
        element = json.dumps((fd, line))
        return await self._conn.rpush(f'jobs.{target}.{jobid}.output', element)
