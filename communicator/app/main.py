#!/usr/bin/python3

import asyncio
import aioredis

from redis_provider import RedisProvider
from connection import Connection
from dispatcher import Dispatcher
from handler import Handler

loop = asyncio.get_event_loop()

async def server(handle_connection: callable):
    server = await asyncio.start_server(handle_connection, '127.0.0.1', 1000)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

async def run_server():
    redisPool = await aioredis.create_redis_pool(('redis', 6379))
    redisProvider = RedisProvider(redisPool)

    async def handle_connection(r: asyncio.StreamReader,
                                w: asyncio.StreamWriter):
        conn = Connection(r, w)
        handler = Handler(redisProvider)
        dispatcher = Dispatcher(handler)
        await dispatcher.run(conn)

    return await server(handle_connection)

loop.run_until_complete(run_server())
