import asyncio

class ConnectionException(Exception):
    pass

class ConnectionClosedException(ConnectionException):
    pass

class Connection:
    def __init__(self, r: asyncio.StreamReader, w: asyncio.StreamWriter):
        self._r = r
        self._w = w

    async def read(self, numBytes: int) -> bytes:
        data = await self._r.read(numBytes)
        if len(data) != numBytes:
            raise ConnectionClosedException()
        return data

    def write(self, data: bytes):
        return self._w.write(data)

    def close(self):
        return self._w.close()

    async def readPrefixedBytes(self, lengthSize: int = 4) -> bytes:
        length = await self.read(lengthSize)
        length = int.from_bytes(length, 'little', signed=False)
        return await self.read(length)

    async def writePrefixedBytes(self, data: bytes, lengthSize: int = 4):
        dataBinLen = len(data)
        await self.write(dataBinLen.to_bytes(lengthSize, 'little', signed=False))
        await self.write(data)

    async def readPrefixedStr(self, lengthSize: int = 4) -> str:
        data = self.readPrefixedBytes(lengthSize)
        return data.decode('UTF-8')

    async def writePrefixedStr(self, data: str, lengthSize: int = 4):
        await self.writePrefixedBytes(self, lengthSize, data.encode('UTF-8'))

    async def readInt(self) -> int:
        return int.from_bytes(await self.read(4), 'little', signed=False)

    def writeInt(self, data: int):
        return self.write(data.to_bytes(4, 'little', signed=False))

    async def readByte(self) -> int:
        return int.from_bytes(await self.read(1), 'little', signed=False)

    def writeByte(self, data: int):
        return self.write(data.to_bytes(1, 'little', signed=False))
